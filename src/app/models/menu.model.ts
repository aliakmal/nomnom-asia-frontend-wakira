import { MenuSection } from './menu_section.model'
export class Menu {
    name: string;
    id: number;
    description: string;
    created_at: string;
    updated_at: string;
    sections:Array<MenuSection>

    constructor(menu_item){
        this.id = menu_item.id;
        this.name = menu_item.name;
        this.description = menu_item.description;
        this.created_at = menu_item.created_at;
        this.updated_at = menu_item.updated_at;
        this.sections = [];
        for(let i in menu_item.sections){
            let section = menu_item.sections[i];
            let menu_section = new MenuSection(section);
            this.sections.push(menu_section);
        }
        
    }

}

