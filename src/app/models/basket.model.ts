import { CartItem } from './cart_item.model'
import { CartComboItem } from './cart_combo.model';
export class Basket {
    items:Array<CartItem>;
    combos:Array<CartComboItem>;
    subtotal:number;
    delivery_charges:number;
    total:number;

    constructor(){
        this.items = [];
        this.combos = [];
        this.delivery_charges = 10;

        this.updatePrices();
    }

    initFromBasket(basket){
        for(let i in basket.items){

            let item = new CartItem();
            item.initFromBasketItem(basket.items[i]);
            this.items.push(item);
        }
        for(let i in basket.combos){
            let combo = new CartComboItem();
            combo.initFromBasketCombo(basket.combos[i]);
            this.combos.push(combo);
        }

        this.delivery_charges = basket.delivery_charges;

        this.updatePrices();
    }

    updatePrices(){
        this.updateSubTotal();
        this.updateTotal();
    }

    updateTotal(){
        this.total = this.subtotal+this.delivery_charges;
    }

    outputItemSelecteds(){
        for(let i in this.items){
            console.log(this.items[i].selected_variation);
        }
    }

    addItem(item){
        for(let i in this.items){
            if(this.items[i].id == item.id){
                this.items[i].increment(item.qty);
                return;
            }
        }
        let cart_item = new CartItem();
        cart_item.initFromBasketItem(item);
        this.items.push(cart_item);
        this.updatePrices();
    }

    count(){
        return (this.items.length + this.combos.length);
    }
    
    removeItems(index){
        this.items.splice(index, 1);
        this.updatePrices();
    }
    
    addCombo(combo){
        for(let i in this.combos){
            if(this.combos[i].id == combo.id){
                this.combos[i].increment(combo.qty);
                return;
            }
        }
        this.combos.push(combo);
        this.updatePrices();
    }

    removeCombos(index){
        this.combos.splice(index, 1);
        this.updatePrices();
    }


    updateSubTotal(){
        this.subtotal = 0;
        for(let i in this.items){
            this.subtotal+= this.items[i].subtotal_price
        }
        for(let i in this.combos){
            this.subtotal+= this.combos[i].subtotal_price
        }
    }

}
