export class SelectOption {
    name: string;
    option_of: string;
    price: number;

    initFromOption(option){
        this.name = option.name;
        this.option_of = option.option_of;
        this.price = option.price;
    }

    setOptionOf(option_of){
      this.option_of = option_of;
    }
}

