import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { Menu } from '../../models/menu.model';
import { ItemVariation } from '../../models/item_variation.model';

import { environment } from '../../../environments/environment';
import 'rxjs/add/operator/toPromise';
import { map } from "rxjs/operators";
import 'rxjs/Rx';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

const httpPostOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'multipart/form-data' })
};

@Injectable()
export class MenuService {
  url:string;
  restaurantID:number;
  merchantID:string;
  results:Object[];
  loading:boolean;
  baseCode:string;

  constructor(private http:HttpClient) { 
    this.results = [];
    this.loading = false;

    this.url = environment.apiUrl;
    this.baseCode = environment.authCode;
    this.restaurantID = environment.restaurantID;
    this.merchantID = environment.merchantID;
  }

  
  getKey(){
    return 'current_restaurant_profile_'+this.restaurantID;
  }

  getMerchantID(){
    return this.merchantID;
  }
  isRestaurantOpen(){
    let data = 'id='+this.restaurantID;
    let url = this.url+'/is_restaurant_open?'+data;
    return this.http.get(url).map(function(response:boolean){ 
      return response;
    });
  }

  applyCouponToOrder(params){
    let key = this.getKey();

    let data = 'restaurant_id='+this.restaurantID;
    data += '&coupon='+params['coupon'];
    data += '&subtotal='+params['subtotal'];
    data += '&phone='+params['phone'];
    data += '&user_id='+params['user_id'];

    let url = this.url+'/validate_coupon?'+data;
    return  this.http.get(url);
  }


  getRestaurantMenu(){
    let key = this.getKey();
    let data = 'restaurant_id='+this.restaurantID;
    let url = this.url+'/menus?'+data;
    let menus = this.http.get(url).map(function(response:any){ 
      return response.map(function(menu_item){
        return new Menu(menu_item);
      });
    });

    return menus;
  }

  getDeliveryLocationsKey(){
    return 'delivery_locations_'+this.restaurantID;
  }

  getHttpAuthOptions(){
    let headers = new HttpHeaders();
    headers = headers.append("Authorization", "Basic " + this.baseCode);
    headers = headers.append("Content-Type", "application/x-www-form-urlencoded");
    return {headers:headers};
  }

  getSessionFromGateway(order){
    let url = 'https://eu-gateway.mastercard.com/api/rest/version/48/merchant/'+this.merchantID+'/session'
    let data = {};
    data['apiOperation'] = 'CREATE_CHECKOUT_SESSION';
    data['order.id'] = order.ref;
    data['order.amount'] = order.total;
    data['order.currency'] = 'AED';
    let response = this.http.post(url, data, this.getHttpAuthOptions());
    return response;
  }

  getSession(order){
    let data = { ref: order.ref, merchant_id:this.merchantID, amount:order.total, auth_code:this.baseCode };
    let url = this.url+'/gateway/session';
    let response = this.http.post(url, {data:data}, httpPostOptions);
    return response;

  }

  getDeliveryLocations(){
    let key = this.getDeliveryLocationsKey();
    let data = 'restaurant_id='+this.restaurantID;
    let url = this.url+'/delivery_locations?'+data;
    let results = this.http.get(url);
    return results;
  }

  saveOrder(order){
    let data = order;
    let url = this.url+'/orders/create';
    let response = this.http.post(url, {data:data}, httpPostOptions);
    return response;
  }

  markOrderCashPayable(order){
    let data = { id: order.id, payment_mode:'cash', state:'pending', payment_status:'' };
    let url = this.url+'/orders/update';
    let response = this.http.post(url, {data:data}, httpPostOptions);
    return response;
  }

  markOrderCardPaid(order){
    let data = { id: order.id, payment_mode:'card', state:'pending', payment_status:'completed' };
    let url = this.url+'/orders/update';
    let response = this.http.post(url, {data:data}, httpPostOptions);
    return response;
  }


}
