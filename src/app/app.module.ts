import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';  //<<<< import it here

import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { UserComponent } from './components/user/user.component';
import { MenuComponent } from './components/menu/menu.component';
import { HistoryComponent } from './components/history/history.component';

import { MenuService } from './services/menu/menu.service';
import { CartService } from './services/cart/cart.service';

import { AppRoutingModule } from './app-routing.module';

import { NgxSmartModalModule } from 'ngx-smart-modal';
import { CheckoutComponent } from './components/checkout/checkout.component';
import { HeaderComponent } from './components/header/header.component';
import { AuthService } from './services/auth/auth.service';
import { ConfirmComponent } from './components/confirm/confirm.component';
import { PayComponent } from './components/pay/pay.component';
import { OrdersComponent } from './components/orders/orders.component';
import { SlugifyPipe } from './shared/slugify.pipe'

import { NgVarDirective } from './directives/ng-var.directive';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    UserComponent,
    MenuComponent,
    HistoryComponent,
    CheckoutComponent,
    HeaderComponent,
    ConfirmComponent,
    PayComponent,
    OrdersComponent,
    SlugifyPipe,
    NgVarDirective    
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    NgxSmartModalModule.forRoot()
  ],
  providers: [MenuService,CartService, AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
