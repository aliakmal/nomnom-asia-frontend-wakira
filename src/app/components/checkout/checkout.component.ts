import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { MenuService } from '../../services/menu/menu.service';
import { CartService } from '../../services/cart/cart.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { NgxSmartModalService } from 'ngx-smart-modal';
import { CartItem } from '../../models/cart_item.model';
import { CartComboItem } from '../../models/cart_combo.model';
import { Basket } from '../../models/basket.model';
import { Order } from '../../models/order.model';
import { Address } from '../../models/address.model';
import { OrderService } from '../../services/order/order.service';
import { AuthService } from '../../services/auth/auth.service';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})

export class CheckoutComponent implements OnInit {
  public basket:Basket;
  public order:Order;
  public address:Address;
  public addresses;//:Array<Address>;
  public delivery_locations;
  public current_zones;
  public loading;
  public error;
  public showAddressForm;

  constructor(
    private router: Router, private auth:AuthService, private menuSelector:MenuService, public ngxSmartModalService: NgxSmartModalService,
    private cartSelector:CartService) { 
      this.loading = false;
      this.error = false;
      this.addresses = [];
      this.showAddressForm = true;
    }

  ngOnInit() {
    this.loading = false;
    this.router.events.subscribe((evt) => {
        if (!(evt instanceof NavigationEnd)) {
            return;
        }
        window.scrollTo(0, 0)
    });
    this.basket = this.cartSelector.getCart();

    console.log('this.basket');console.log(this.basket);
    this.order = new Order();
    if(this.auth.isLoggedIn()){
      this.order.user_id = this.auth.getUserId();
    }
    
    this.order.attachBasket(this.basket);
    if(this.auth.isLoggedIn()){
      let user = this.auth.getLoggedInUser();
      this.order.attachUser(user);
      console.log(user);
      this.auth.getAddresses(user).subscribe(
        data => { 
          for(let i in data){
            this.addresses.push(new Address(data[i]));
          }
        },
        err => console.error(err),
        () => console.log('done loading foods')
      );
      if(this.addresses.length > 0){
        this.showAddressForm = false;
      }
    }

    this.delivery_locations = [];
    this.loadDeliveryLocations();
    
    this.current_zones = [];
  }

  addEmptyAddress(){
    this.order.address = new Address({});
    this.showAddressForm = true;
  }

  goBackToMenu(){
    this.router.navigate(['']);
  }

  setCurrentZonesSelectable(){
    for(let i in this.delivery_locations){
      if(this.delivery_locations[i].id == this.order.address.city_id){
        this.current_zones = this.delivery_locations[i].zones;
      }

    }
  }

  applyCouponToOrder(){
    let params = {};
    params['coupon'] = this.order.coupon_code;
    params['subtotal'] = this.order.subtotal;
    params['phone'] = this.order.customer_phone;

    if(!this.auth.isLoggedIn()){
      params['user_id'] = 0;
    }else{
      params['user_id'] = this.auth.getUserId();
    }


    this.menuSelector.applyCouponToOrder(params).subscribe(
      data => { 
        if(data['success'] == 0){
          this.order.clearCouponCode();
          alert(data['message']);
          return;
        }

        this.order.applyCouponCode(data);
        alert('Coupon has been applied');
        return;
      },
      err => {
        console.error(err)
        this.order.clearCouponCode();
        alert('Something went wrong - coupon could not be applied');
      },
      () => console.log('done loading foods')
    );
  }

  initializeAddress(){
    this.order.address = new Address({city_id:this.delivery_locations[0].id, city_name:this.delivery_locations[0].name});
  }

  loadDeliveryLocations(){
    let results = localStorage.getItem(this.menuSelector.getDeliveryLocationsKey());
    if(!results)
    {
      this.menuSelector.getDeliveryLocations().subscribe(
        data => { 
          this.delivery_locations = data;
          this.initializeAddress();
          localStorage.setItem(this.menuSelector.getDeliveryLocationsKey(), JSON.stringify(data));
        },
        err => console.error(err),
        () => console.log('done loading foods')
      );
    }else{
      results = JSON.parse(results);
      this.delivery_locations = results;
      this.initializeAddress();
    }
   
  }

  isOnlinePayment(){
    return this.order.isOnlinePayment();
  }

  initForOnlinePayment(){
    this.order.initForOnlinePayment();
  }

  removeAddress(address){
    for(let i in this.addresses){
      if(this.addresses[i].id == address.id){
        this.auth.removeAddress(address.id).subscribe(
          data => { 
            this.addresses.splice(+i, 1);
            this.auth.refreshUserDetails();
          },
          err => console.error(err),
          () => console.log('done loading foods')
        );

        return;
        
      }
    }
  }

  setCurrentAddress(address){
    this.order.address = new Address(address);
    this.setCurrentZonesSelectable();
    
    this.showAddressForm = true;
  }

  validate(){
    let str = [ 'house_no', 
                'street', 
                'address_1', 
                'zone_name', 
                'city_name'];
    for(let i in str){
      if(this.order.address[str[i]].trim() == ''){
        return false;
      }
    }

    str = [ 'customer_name', 
            'customer_phone'];

    for(let j in str){
      if(this.order[str[j]].trim() == ''){
        return false;
      }
    }


    var regex_phone = /^(?:\+971|00971|0)(?:50|51|52|55|56)[0-9]{7}$/;

    if(!regex_phone.test(this.order['customer_phone'])){
      return false;
    }

    return true;
  }

  confirmOrder(){
    this.order.zone_id = this.order.address.zone_id;
    if(this.validate()==false){
      this.error = true;
    }

    this.loading = true;



    if(this.isOnlinePayment()){
      this.initForOnlinePayment();
    }else{
      this.order.initForOfflinePayment();
    }
    
    this.menuSelector.saveOrder(this.order).subscribe(
      data=>{
        this.loading = false;
        this.error = false;

        this.cartSelector.saveOrder(data);
        if(this.isOnlinePayment()){
          this.router.navigate(['pay']);
        }else{
          this.cartSelector.emptyCart();

          this.router.navigate(['confirm']);
        }
      },
      err => {
        this.loading = false;
        console.log('err');
        this.error = true;
        console.log(err);
      },
      () => console.log('Done')
    );
  }

}
